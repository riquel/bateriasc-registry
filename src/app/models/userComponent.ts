export class userComponent {
    firstName : String
    lastName : String
    email : String
    password : String
    phone : Number
    age : String
    instrument : String
    istocar : Boolean
    tiempotocando : String
    isprocedebanda : Boolean
    bandaprocedencia : String
    isaltasolicitada : Boolean
    token : String
}
