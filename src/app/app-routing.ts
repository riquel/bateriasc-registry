import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home';
import { LoginComponent } from './components/login';
import { RegisterComponent } from './components/register';
import { AccountverifyComponent } from './components/accountverify'
import { EstadoSolicitud } from './components/estado-solicitud'
import { AuthGuard, SummitFormGuard, NotSummitFormGuard } from './helpers';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard, SummitFormGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'estado-solicitud', component: EstadoSolicitud , canActivate: [AuthGuard, NotSummitFormGuard]},
    { path: 'account/verify', component: AccountverifyComponent },
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);