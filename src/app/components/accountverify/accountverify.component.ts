import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService, ComponentService } from '../../services';

@Component({
  selector: 'app-accountverify',
  templateUrl: './accountverify.component.html'
})
export class AccountverifyComponent implements OnInit {

  constructor( private activatedRoute: ActivatedRoute,  
               private userService: ComponentService,
               private alertService: AlertService,
               private router: Router,
              ) {
                  this.activatedRoute.queryParams.subscribe(params => {
                    let activateToken = params['activateToken'];
                    if(activateToken){
                        this.userService.activateAccount(activateToken)
                        .pipe(first())
                        .subscribe(
                            (data) => {
                                this.alertService.success(data['message'], false);
                                setTimeout(() => {
                                  this.router.navigate(["/login"]);
                              }, 5000);           
                            },
                            (error) => {
                                this.alertService.error(error);
                            });
                    }else{
                      this.router.navigate(["/"]);
                    }
                  });
                }
                
  ngOnInit() {}
}
