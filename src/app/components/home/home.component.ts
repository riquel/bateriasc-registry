import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService, ComponentService, AuthenticationService } from '../../services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  newComponent: FormGroup;
  loading = false;
  submitted = false;
  tiempoTocandoDisabled = true;
  procedeBandaDisabled = true;

     constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        public authenticationService: AuthenticationService,
        private componentService: ComponentService,
        private alertService: AlertService,
      
    ) {}

    toggleSabeTocar(){   
      this.tiempoTocandoDisabled = !this.tiempoTocandoDisabled;
        if(this.tiempoTocandoDisabled){
          this.newComponent.controls['tiempotocando'].disable()
          this.newComponent.controls['tiempotocando'].reset()
        } else {
          this.newComponent.controls['tiempotocando'].enable()
        }
    }

    toggleProcedeBanda(){   
        this.procedeBandaDisabled = !this.procedeBandaDisabled;
        if(this.procedeBandaDisabled){
          this.newComponent.controls['bandaprocedencia'].disable() 
          this.newComponent.controls['bandaprocedencia'].reset()
        } else {
          this.newComponent.controls['bandaprocedencia'].enable()
        }
    }

    get f() { return this.newComponent.controls; }

    ngOnInit() {
      this.newComponent = this.formBuilder.group({
        phone: ['', [Validators.required, Validators.pattern("[0-9 ]{9}")]],
        age: ['', Validators.required],
        instrument: ['', Validators.required],
        istocar: ['false', Validators.required],
        tiempotocando: [{value: '', disabled: true}, Validators.required],
        isprocedebanda: ['false', Validators.required],
        bandaprocedencia: [{value: '', disabled: true}, Validators.required]
      });
    }

    onSubmit() {
      this.submitted = true;
      if (this.newComponent.invalid) {
          return;
      }
      this.loading = true;
      this.componentService.componentRegister(this.newComponent.value)
      .pipe(first())
      .subscribe(
          (data) => {
              this.alertService.success(data['message'], true);
              this.authenticationService.updateComponent();
              this.router.navigate(['/estado-solicitud']);
          },
          (error) => {
              this.alertService.error(error);
              this.loading = false;
          });
    }
}