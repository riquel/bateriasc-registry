import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { userComponent } from '../models';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<userComponent>;
    public currentUser: Observable<userComponent>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<userComponent>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): userComponent {
        return this.currentUserSubject.value;
    }

    updateComponent(){
        let userComponent = JSON.parse(localStorage.getItem('currentUser'));
        userComponent.isaltasolicitada = true;
        localStorage.setItem('currentUser', JSON.stringify(userComponent));
        this.currentUserSubject.next(userComponent)
    }

    login(email: string, password: string) {
        return this.http.post<any>( `${environment.apiUrlBase}/component/sigin`, { email, password })
            .pipe(map(userComponent => {
                if (userComponent && userComponent.token) {
                    localStorage.setItem('currentUser', JSON.stringify(userComponent));
                    this.currentUserSubject.next(userComponent);
                }

                return userComponent;
            }));
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}