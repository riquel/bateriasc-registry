import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { userComponent } from '../models';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ComponentService {
    constructor(private http: HttpClient) { }

    register(component: userComponent) {
        return this.http.post(`${environment.apiUrlBase}/component`, component);
    }

    activateAccount(activateToken: String){
        return this.http.put(`${environment.apiUrlBase}/account/verify`, {activateToken :activateToken});
    }

    componentRegister(component: userComponent){
        return this.http.put(`${environment.apiUrlBase}/component`, component);
    }
}