'use strict'
const express = require("express")
const bodyParser = require("body-parser")
const app = express()
const apiRoutes = require("./server/routes/api")
const cors = require('cors')
const config = require("./config")

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

var corsOptions = {
    origin: function(origin, callback){
          var isWhitelisted = config.ORIGIN_ALLOW_LIST.indexOf(origin) !== -1;
          callback(null, isWhitelisted);
    },
    credentials:true
  }

app.use(cors(corsOptions));
app.use('/api', apiRoutes)

module.exports = app
