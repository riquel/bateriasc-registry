'use strict'

const mongoose = require("mongoose")
const app = require("./server")
const config = require("./config")
const componentCtrl = require("./server/controllers/component")
const Component = require("./server/models/component")

mongoose.connect(config.db, {user: 'onucloud_bateriaschuelva', pass: '8ybq4GZ7'}, (err, res) => {
    if (err) {
        console.log(`error al conectar a la base de datos: ${err}`)
    }
    console.log('Conexión a la base de datos establecida...')
    
    app.listen(config.port, () => {
        console.log(`API ejecutando en http://localhost:${config.port}`)
        let component = new Component()
        component.firstName = config.FIRSTNAME_ADMIN
        component.lastName = config.LASTNAME_ADMIN
        component.email = config.EMAIL_ADMIN
        component.password = config.ADMIN_PASSWORD
        component.isActiveAccount = true
        component.levelRol = config.LEVEL_ADMIN
        componentCtrl.createUserAdmin(component);
    })
    
})





