'use strict'

const express = require("express")
const componentCtrl = require("../controllers/component")
const api = express.Router()
const authActiveAccount = require("../middleweares/authActiveAccount")
const authValidateToken = require("../middleweares/authValidateToken")
const authAdmin= require("../middleweares/authAdmin")
/*
Endpoints encargados de gestionar los componentes.
Esta rutas seran accesible dependiendo del role que tenga cada usuario 
que realiza la petición.
*/
api.get('/component', authAdmin, componentCtrl.getComponents)
api.delete('/component/:componentId', authAdmin, componentCtrl.deleteComponent)
api.put('/account/verify', authActiveAccount, componentCtrl.activateAccount)
api.put('/component', authValidateToken, componentCtrl.updateComponent)
api.post('/component/sigin', componentCtrl.signIn)
api.post('/component', componentCtrl.saveComponent)
/*
Exportación del módulo para hacer las rutas accesibles desde otras 
partes de la aplicación.
*/
module.exports = api
