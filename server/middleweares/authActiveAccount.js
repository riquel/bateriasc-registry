'use stric'

const serviceToken = require("../services/token")
const config = require("../../config")
/*
Función encargada de realizar la lógica de comprobar 
si el token de activación de la cuenta es válido. 
Si es válido, lo mete como parametro en la request 
para que lo procese el controller.
*/
function isValidActivateToken(req, res, next) {
    if (!req.body.activateToken) {
        return res.status(401).send({ message: 'No tienes autorización' })
    }
    const token = req.body.activateToken;
    serviceToken.decodeToke(token)
        .then(response => {
            req.email = response.sub
            next()
        })
        .catch(response => {
            return res.status(response.status)
        })
}
/*
Exportación de los métodos para hacerlos accesibles desde otras 
partes de la aplicación.
*/
module.exports = isValidActivateToken
