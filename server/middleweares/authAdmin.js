'use stric'

const services = require("../services/token")
const config = require("../../config")
/*
Función encargada de realizar la lógica de comprobar 
si el token es un token váido de administración. 
Si es válido, lo mete como parametro en la request 
para que lo procese el controller.
*/
function isAdmin(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(401).send({ message: 'No tienes autorización' })
    }
    const token = req.headers.authorization.split(" ")[1]
    services.decodeToke(token)
        .then(response => {
            if (response.levelRol != config.LEVEL_ADMIN) return res.status(403).send({ message: 'No tienes autorización' })
            req.user = response.sub
            next()
        })
        .catch(response => {
            return res.status(401).send({ message: 'No tienes autorización' })
        })
}
/*
Exportación de los métodos para hacerlos accesibles desde otras 
partes de la aplicación.
*/
module.exports = isAdmin