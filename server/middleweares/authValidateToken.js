'use stric'

const serviceToken = require("../services/token")
const config = require("../../config")
/*
Función encargada de realizar la lógica de comprobar 
si el token es válido. Si es válido, mete el email 
como parametro en la request para que lo procese el controller.
*/
function isvalidToken(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(401).send({ message: 'No tienes autorización' })
    }

    const token = req.headers.authorization.split(" ")[1]
    serviceToken.decodeToke(token)
        .then(response => {
            req.email = response.sub
            next()
        })
        .catch(response => {
            return res.status(401).send({ message: 'No tienes autorización' })
        })
}
/*
Exportación de los métodos para hacerlos accesibles desde otras 
partes de la aplicación.
*/
module.exports = isvalidToken
