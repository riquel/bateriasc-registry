'use strict'

const mongoose = require("mongoose")
const bcrypt = require("bcrypt-nodejs")
const Schema = mongoose.Schema
/*
Contante donde se define la esctrutura de un componente del documento de mongodb
*/
const ComponentSchema = new Schema({
    firstName : String,
    lastName: String,
    email: { type : String, unique : true, index: { unique: true }},
    password : String,
    phone : Number,
    age : String,
    instrument : String,
    istocar : { type: Boolean, default: false },
    tiempotocando: String,
    isprocedebanda: { type: Boolean, default: false },
    bandaprocedencia: String,
    isaltasolicitada: { type: Boolean, default: false },
    isActiveAccount: { type: Boolean, default: false },
    levelRol : { type: Number, default: 0 },
    signupDate : { type : Date, default : Date.now() },
    lastLogin : Date
})
/*
Función encargada de realizar la lógica de  actualizar y encriptar 
la contrasela de un componente. Si ha sido modificada 
la guarda encriptada.
*/
ComponentSchema.pre('save', function (next) {
    let component = this
    if (!this.isModified('password')) return next()
    bcrypt.genSalt(10, (err, salt) => {
        if (err) return next()
        bcrypt.hash(component.password, salt, null, (err, hash) => {
            if (err) return next(err)
            component.password = hash
            next()
        })
    })
})
/*
Función encargada de realizar la lógica de comparar si dos password son 
iguales estando encriptadas.
*/
ComponentSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        return cb(null, isMatch);
    });
};
/*
Exportación del modelo para hacerlos accesibles desde otras 
partes de la aplicación.
*/
module.exports = mongoose.model('Component', ComponentSchema)