'use strict'

const jwt = require("jwt-simple")
const nodemailer = require('nodemailer');
const moment = require("moment")
const config = require("../../config")
/*
Función encargada de realizar la lógica de crear un JWT 
con el email del componente en el payload del token 
encriptado con una clave.
*/
function createToken(component) {
    const payload = {
        sub: component.email,
        iat: moment().unix(),
        exp: moment().add(1, 'days').unix()
    }

    return jwt.encode(payload, config.SECRET_TOKEN)
}
/*
Función encargada de realizar la lógica de crear un JWT 
con el role de administración en el payload del token 
encriptado con una clave.
*/
function createTokenAdmin(component) {
    const payload = {
        levelRol: component.levelRol,
        iat: moment().unix(),
        exp: moment().add(1, 'days').unix()
    }

    return jwt.encode(payload, config.SECRET_TOKEN)
}

/*
Función encargada de realizar la lógica de desencriptar el JWT con la clave, 
realiza las comprobaciones de caducidad de de dicho token.
*/
function decodeToke(token) {
    const decode = new Promise((resolve, reject) => {
        try {
            const payload = jwt.decode(token, config.SECRET_TOKEN)
            if (payload.exp <= moment.unix()) {
                reject({
                    status: 401,
                    message: 'El token ha expirado'
                })
            }

            resolve(payload)
        }
        catch (err) {
            reject({
                status: 500,
                message: 'Invalid Token'
            })
        }
    })

    return decode
}
/*
Exportación de los métodos para hacerlos accesibles desde otras 
partes de la aplicación.
*/
module.exports = {
    createToken,
    decodeToke,
    createTokenAdmin
}