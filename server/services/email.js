'use strict'

const nodemailer = require('nodemailer');
const hbs = require("nodemailer-express-handlebars")
const config = require("../../config")
const tokenService = require("../services/token")
/*
Función encargada de realizar la lógica de mandar un email de 
verificación de cuenta a partir de una plantilla hbs.
*/
function sendVerifyEmail(component){

    let transporter = nodemailer.createTransport(config.SMPT_CONFIG);

    let mailOptions = {
        from: '"A.M. Santa cruz Huelva" <bateriasantacruzhuelva@gmail.com',
        to: component.email,
        subject: `Bateriasc E-mail Verification - <${component.email}>`,
        template: 'verificacionEmail',
        context: { baseUrl : config.BASE_URL,
                   firstname : component.firstName,
                   activateToken : tokenService.createToken(component)
                 }
    }

    let handlebarOptions = {
        viewEngine: {
          extName: '.hbs',
          partialsDir: 'server/email_templates',
          layoutsDir: 'server/email_templates',
          defaultLayout: 'verificacionEmail.hbs',
        },
        viewPath: 'server/email_templates',
        extName: '.hbs',
      };

    transporter.use('compile', hbs(handlebarOptions));

    let info = transporter.sendMail(mailOptions, function(error, response){
        if (error) {
            console.log(error);
        }else{
            console.log(response);
        }
    });

}
/*
Función encargada de realizar la lógica de mandar un email de 
notificación de solicitud de alta a la administración a partir 
de una plantilla hbs.
*/
function sendEmailNotificacionSolicitud(component){

    let transporter = nodemailer.createTransport(config.SMPT_CONFIG);

    let mailOptions = {
        from: '"A.M. Santa cruz Huelva" <bateriasantacruzhuelva@gmail.com',
        to: config.EMAILS_NOTIFICACION_COMPONENTE,
        subject: `Petición de solicitud de alta - <${component.email}>`,
        template: 'informacionSolicitud',
        context: { baseUrl : config.BASE_URL,
                   firstName : component.firstName,
                   lastName : component.lastName,
                   email : component.email,
                   phone : component.phone,
                   age : component.age,
                   instrument : component.instrument,
                   istocar : component.istocar ? 'Si' : 'No',
                   tiempotocando : component.istocar ? component.tiempotocando : '--',
                   isprocedebanda : component.isprocedebanda ? 'Si' : 'No',
                   bandaprocedencia : component.isprocedebanda ? component.bandaprocedencia : '--'
                 }
    }

    let handlebarOptions = {
        viewEngine: {
          extName: '.hbs',
          partialsDir: 'server/email_templates',
          layoutsDir: 'server/email_templates',
          defaultLayout: 'informacionSolicitud.hbs',
        },
        viewPath: 'server/email_templates',
        extName: '.hbs',
      };

    transporter.use('compile', hbs(handlebarOptions));

    let info = transporter.sendMail(mailOptions, function(error, response){
        if (error) {
            console.log(error);
        }else{
            console.log('Se manda email de notificación')
            console.log(response);
        }
    });

}
/*
Función encargada de realizar la lógica de mandar un email de 
confirmación de solicitud de alta al usuario a partir 
de una plantilla hbs.
*/
function sendEmailConfirmacionSolicitud(component){
  
    let transporter = nodemailer.createTransport(config.SMPT_CONFIG);

    let mailOptions = {
        from: '"A.M. Santa cruz Huelva" <bateriasantacruzhuelva@gmail.com',
        to: component.email,
        subject: `Confirmación de solicitud de incorporación - <${component.email}>`,
        template: 'confirmacionSolicitud',
        context: { baseUrl : config.BASE_URL,
                   firstName : component.firstName,
                   lastName : component.lastName,
                   email : component.email,
                   phone : component.phone,
                   age : component.age,
                   instrument : component.instrument,
                   istocar : component.istocar ? 'Si' : 'No',
                   tiempotocando : component.istocar ? component.tiempotocando : '--',
                   isprocedebanda : component.isprocedebanda ? 'Si' : 'No',
                   bandaprocedencia : component.isprocedebanda ? component.bandaprocedencia : '--'
                 }
    }

    let handlebarOptions = {
        viewEngine: {
          extName: '.hbs',
          partialsDir: 'server/email_templates',
          layoutsDir: 'server/email_templates',
          defaultLayout: 'confirmacionSolicitud.hbs',
        },
        viewPath: 'server/email_templates',
        extName: '.hbs',
      };

    transporter.use('compile', hbs(handlebarOptions));

    let info = transporter.sendMail(mailOptions, function(error, response){
        if (error) {
            console.log(error);
        }else{
            console.log('Se manda email de confirmación')
            console.log(response);
        }
    });

}
/*
Exportación de los métodos para hacerlos accesibles desde otras 
partes de la aplicación.
*/
module.exports = {
    sendVerifyEmail,
    sendEmailNotificacionSolicitud,
    sendEmailConfirmacionSolicitud
}