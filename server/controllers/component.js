'use strict'

const Component = require("../models/component")
const tokenService = require("../services/token")
const emailservice = require("../services/email")
const config = require("../../config")

/*
Función encargada de realizar la lógica de listar todos los componentes 
dados de altas en la base de datos.
*/
function getComponents(req, res) {
    Component.find({}, (err, components) => {
        if (err) return res.status(500).send({ message: 'Error al realizar la peticón' })
        if (!components) return res.status(404).send({ message: 'No existen usuarios' })
        return res.status(200).send({ components })
       
    })
}
/*
Función encargada de realizar la lógica de guardar un componente en la 
base de datos a partir de los datos proporcinados en la petición.
*/
function saveComponent(req, res) {
    let component = new Component()
    component.firstName = req.body.firstName
    component.lastName = req.body.lastName
    component.email = req.body.email
    component.password = req.body.password

    component.save((err) => {
        if (err) return res.status(500).send({ message: 'El usuario ya existe' })
        emailservice.sendVerifyEmail(component)

        return res.status(200).send({ message: 'Usuario creado correctamente, verifique su correo electronico para activar su cuenta' })
    })
}
/*
Función encargada de realizar la lógica de actualizar un componente en la
base de datos a partir de los datos proporcinados en la petición.
*/
function updateComponent(req, res) {
    let update = req.body
    console.log(update);
    Component.findOne({ email: req.email }, (err, component) => {
        if (!component) return res.status(404).send({ message: 'El usuario no existe' })
        component.phone = update.phone
        component.age = update.age
        component.instrument = update.instrument
        component.istocar = update.istocar
        component.tiempotocando = update.tiempotocando
        component.isprocedebanda = update.isprocedebanda
        component.bandaprocedencia = update.bandaprocedencia
        component.isaltasolicitada = true;

        component.save((err, componentUpdated) => {
            if (err) return res.status(500).send({ message: 'Error al actualizar el usuario' })
            emailservice.sendEmailNotificacionSolicitud(component)
            emailservice.sendEmailConfirmacionSolicitud(component)
            return res.status(200).send({ message: 'Solicitud de alta recibida correctamente' })


        })
    })
}
/*
Función encargada de realizar la lógica de borrar un componente en la 
base de datos a partir del id proporcinado en la petición.
*/
function deleteComponent(req, res) {
    let componentId = req.params.componentId
    Component.findById(componentId, (err, component) => {
        if (err) return res.status(500).send({ message: 'Error al borrar el usuario:' })
        if(!component) return res.status(404).send({ message: 'El usuario no existe' })
        
        component.remove(err => {
            return res.status(200).send({ message: 'El usuario ha sido borrado' })
        })
    })
}
/*
Función encargada de realizar la lógica de activar la cuenta de un componente en la 
base de datos a partir del email proporcinado en la petición.
*/
function activateAccount(req, res){
    Component.findOne({ email: req.email }, (err, component) => {
        if (!component) return res.status(404).send({ message: 'Usuario no existe' })
        component.isActiveAccount = true;
        component.save((err, componentUpdated) => {
            if (err) return res.status(500).send({ message: 'Error al activar la cuenta'})

            return res.status(200).send({ message: 'Su dirección de correo se ha confirmado correctamente' })
        })
    })
}
/*
Función encarga de crear el usuario de administración. Primero comprueba si existe,
y de no ser asi lo crea a partir de datos estáticos que se le pasa como parametro 
al arrancar la api.
*/
function createUserAdmin(userAdmin) {
    Component.findOne({levelRol: userAdmin.levelRol}, (err, component) => {
        if (!component) {
            userAdmin.save((err, componentUpdated) => {
                if (!err) {
                    console.log('Usuario de administración creado correctamente');
                }else{
                    console.log('Error creando el usuario de Administración');
                }
            })
        }
    })
}
/*
Función encargada de realizar la lógica de autenticación de usuarios 
contra la base de datos. Si el usuario existe y la password es correcta, 
generará un token con el nivel de role que tiene ese usuario. Dicho nivel 
de role se utilizará en futuras peticiones y el middleware, determinará a 
que rutas de la aplicación tiene acceso el usuario autenticado.
*/
function signIn(req, res) {
    Component.findOne({ email: req.body.email }, (err, component) => {
        if (err) return res.status(500).send({ message: err })
        if (!component) return res.status(404).send({ message: 'Usuario o contraseña incorrecto' })
        if (!component.isActiveAccount) return res.status(403).send({ message: 'Cuenta no activada. Por favor revise su corrreo electrónico para activar su cuenta' })
        component.comparePassword(req.body.password, function(err, isMatch) {
            if (err) return res.status(500).send({ message: 'Error al autentificar el usuario' })
            if (!isMatch) return res.status(401).send({ message: 'Usuario o contraseña incorrecto' })
 
            return res.status(200).send({ id: component.id,
                                          firstName: component.firstName,
                                          lastname: component.lastName,
                                          isaltasolicitada : component.isaltasolicitada,
                                          token: component.levelRol == config.LEVEL_ADMIN ? tokenService.createTokenAdmin(component) : tokenService.createToken(component)})
                                        });
    })
}
/*
Exportación de los métodos para hacerlos accesibles desde otras 
partes de la aplicación.
*/
module.exports = {
    getComponents,
    saveComponent,
    updateComponent,
    deleteComponent,   
    activateAccount,
    createUserAdmin,
    signIn
}